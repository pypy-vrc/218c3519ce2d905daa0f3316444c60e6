const crypto = require("crypto");
const fs = require("fs");

//
const iv = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

function decryptAESWithKey(key, data) {
  const d = crypto.createDecipheriv("aes-256-cbc", key, iv);
  return Buffer.concat([
    d.update(Buffer.from(data, "base64")),
    d.final(),
  ]).toString();
}

function encryptAESWithKey(key, data) {
  const c = crypto.createCipheriv("aes-256-cbc", key, iv);
  return Buffer.concat([c.update(data), c.final()]).toString("base64");
}

// authy
const password = "123qweASD";
const salt = "";
const verification = "";

const key = crypto.pbkdf2Sync(
  password,
  Buffer.from(salt, "hex"),
  100000,
  32,
  "sha1"
);

// if (encryptAESWithKey(key, salt) === verification) {
//   console.log(decryptAESWithKey(key, fs.readFileSync("crypto.txt").toString()));
// }

function base32Decode(s) {
  let value = 0;
  let bits = 0;
  let index = 0;
  const output = new Uint8Array(Math.floor(s.length * 0.625));

  for (const c of s) {
    value = (value << 5) | "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".indexOf(c);
    bits += 5;

    if (bits >= 8) {
      output[index++] = (value >>> (bits - 8)) & 255;
      bits -= 8;
    }
  }

  return Buffer.from(output);
}

// https://datatracker.ietf.org/doc/html/rfc6238
function hotp(seed, steps) {
  const hash = crypto
    .createHmac("sha1", seed)
    .update(Buffer.from(steps.toString(16).padStart(16, "0"), "hex"))
    .digest("hex");
  const offset = parseInt(hash.at(-1), 16) * 2;
  return parseInt(hash.slice(offset, offset + 8), 16) & 0x7fffffff;
}

// https://datatracker.ietf.org/doc/html/rfc4226
function totp(seed, digits = 6, timeStep = 30) {
  const time = Math.floor(Date.now() / 1000);
  return hotp(seed, Math.floor(time / timeStep))
    .toString()
    .slice(-digits);
}

// otpauth://<type>/<label>?secret=<base32secret>&issuer=<issuer>

setInterval(function () {
  const secret = "";
  const seed = base32Decode(secret);
  console.log(totp(seed));
}, 1000);